import nose
from nose.tools import *

from app import app

test_app = app.test_client()


def test_homepage():
    response = test_app.get('/')
    eq_(response.status_code, 200)
    html_body = response.data
    eq_(html_body, 'Hello world!')

    names = ['Jim', 'Mary', 'John']
    for name in names:
        response = test_app.get('/hello/%s' % name)
        eq_(response.status_code, 200)
        html_body = response.data
        eq_(html_body, 'Hello %s!' % name)
