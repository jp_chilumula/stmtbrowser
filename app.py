from flask import Flask
from flask import render_template, request
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATA_URI'] = 'postgresql+psycopg2://jp@localhost/jp1'
db = SQLAlchemy(app)


class Files(db.Model):
    __tablename__ = 'files'

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(80), unique=True)

    def __init__(self, filename):
        self.filename = filename

    def __repr__(self):
        return '<File %r>' % self.filename


@app.route('/')
def homepage():
    return render_template('index.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    info = 'No file uploaded'
    if request.files:
        info = ''

        for data in request.files['upload_file'].read():
            info += data

        file = Files(request.files['upload_file'].filename)
        db.session.add(file)
        db.session.commit()

    return render_template('upload.html', upload_data=info)

if __name__ == '__main__':
    app.run()
